<?php

namespace App\Entity;

use App\Repository\TransactionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TransactionRepository::class)
 */
class Transaction
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ppaymentIntent;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $record;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPpaymentIntent(): ?string
    {
        return $this->ppaymentIntent;
    }

    public function setPpaymentIntent(string $ppaymentIntent): self
    {
        $this->ppaymentIntent = $ppaymentIntent;

        return $this;
    }

    public function getRecord(): ?string
    {
        return $this->record;
    }

    public function setRecord(?string $record): self
    {
        $this->record = $record;

        return $this;
    }
}
