<?php

namespace App\Controller;


use App\Entity\Transaction;
use Doctrine\Persistence\ManagerRegistry;
use Stripe\Customer;
use Stripe\PaymentIntent;
use Stripe\PaymentMethod;
use Stripe\StripeClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class stripeController extends AbstractController
{
    public function test(Request $request): JsonResponse
    {
        return response()->json([
            "message" => "Bonjour tout se passe bien",
            "success" => true
        ]);
    }

    public function getNewClient(): StripeClient
    {
        return new StripeClient(
           'sk_test_51MQT2pKeHcCj15fGHIkpa8eHlLQ68CavmG2RlJEozxgUITLzLmbOpNevt9k4HdmSo3N5koRExi2uANmF11meLPIR00jZa8EdHl'
        );
    }

    public function createPaymentMethod(): PaymentMethod
    {
        $stripe = $this->getNewClient();

        $paymentMethod = $stripe->paymentMethods->create([
            'type' => 'card',
            'card' => [
                'number' => '4242424242424242',
                'exp_month' => 8,
                'exp_year' => 2024,
                'cvc' => '314',
            ],
        ]);
        return $paymentMethod;
    }

    public function createCustomer($name, $email): Customer
    {

        $stripe = $this->getNewClient();

        $customer = $stripe->customers->create([
            'name' => $name,
            //'description' => 'My First Test Customer',
            'email' => $email,
            //'payment_method' => 'pm_1MQTzwKeHcCj15fGpK5JJh2o'
        ]);
        return $customer;
    }

    public function createPaymentIntent(Request $request): Response
    {
        $parameters = json_decode($request->getContent(), true);

        $name = $parameters['name'];
        $email = $parameters['email'];

        $stripe = $this->getNewClient();

        //$paymentMethod = $this->createPaymentMethod();

        $customer = $this->createCustomer($name, $email);

        $paymentIntent = $stripe->paymentIntents->create([
            'amount' => 2000 ."00",
            'currency' => 'eur',
            'payment_method_types' => ['card'],
            'off_session' => false,
            'customer' => $customer->id,
        ]);
        return $this->json(json_encode($paymentIntent));
    }

    public function retrievePaymentIntent($clientSecret): PaymentIntent
    {

        $stripe = $this->getNewClient();

        $paymentIntent = $stripe->paymentIntents->retrieve(
            $clientSecret,
            []
        );
        return $paymentIntent;
    }

    public function confirmPaymentIntent(Request $request, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();
        $clientSecret = null;
        $paymentMethod = null;

        $stripe = $this->getNewClient();

        $parameters = json_decode($request->getContent(), true);

        $clientSecret = $parameters['client_secret'];

        $paymentMethod = $parameters['payment_method'];


        $paymentIntent = $this->retrievePaymentIntent($clientSecret);
        $transaction = null;
        if ($paymentIntent->status == "succeeded") {
            $transaction = new Transaction();
            $transaction->setPpaymentIntent($paymentIntent->id);
            $transaction->setRecord(json_encode($paymentIntent));
            $transaction->setStatus($paymentIntent->status);
            $entityManager->persist($transaction);
        }

        $entityManager->flush();

        return $this->json(json_encode($transaction));
    }

  

}
