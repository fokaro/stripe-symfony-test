<?php

namespace App\Controller;

use App\Entity\Product;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductController extends AbstractController
{

    public function createProduct(Request $request, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();

        $parameters = json_decode($request->getContent(), true);

        $name = $parameters['name'];
        $price = $parameters['price'];

        $product = new Product();
        // This will trigger an error: the column isn't nullable in the database
        $product->setName($name);
        // This will trigger a type mismatch error: an integer is expected
        $product->setPrice($price);

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($product);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();

        $status = 200;

        $headers = [];

        $result = [
            'id' => $product->getId(),
            'name' => $product->getName(),
            'price' => $product->getPrice()
        ];

        return $this->json($result);

        //return new JsonResponse(array("John", "Mary", "Peter", "Sally"), $status, $headers);
       //return $this->json($product->getName(), $status, $headers);
    }
}
